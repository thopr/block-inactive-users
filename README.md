# Block inactive users

Provides a script to block all inactive users using the GitLab API, given a configurable inactivity period. Can be run as scheduled pipeline to continuously block inactive users.

**Caution**: Blocked users are unable to sign into GitLab. They have to be manually unblocked by an admin. To avoid this wait time for users to be unblocked and the inefficient manual interactions for admins, it is recommended to [deactivate them instead](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-auto-deactivate-inactive-users). Note that deactivation depends on the user to be inactive for 90 days.

## Configuration

- Export / fork repository.
- Add a GitLab **admin** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an admin token to be able to list all users and deactivate them.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- Configure `INACTIVITY_PERIOD`: This is the period in days that users have to be inactive before being blocked.
- Notice the Job is tagged `local` for testing purposes. Make sure your runners pick it up.
- Schedule the pipeline to run it weekly or your desired interval, using Pipelines -> Schedules

## Usage

`python3 auto-deactivate-inactive-users.py $GITLAB_URL $GITLAB_TOKEN $INACTIVITY_PERIOD [--no-dryrun] [--allowed-list]`

## Parameters

- `--no-dryrun`: Not dryrun mode. Deactivate users and compile the CSV report.
- `--allowed-list`: List of usernames to not block / ignore. Should point to a file with one username per line.

## What does it do?

* Request all users using https://docs.gitlab.com/ee/api/users.html#for-admins
* For all active users that are not bots:
  * If user did not log in for `INACTIVITY_PERIOD` days or has never logged in: block user **unless**
  * We are doing a dryrun **or**
  * The user is in the `--allowed-list`
* Write a CSV report of all user to be blocked / that were blocked (`--no-dryrun`).

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. **Always run it in dryrun mode first, to understand the impact to your users.**

This script produces data file artifacts containing user names. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.
